FROM circleci/node:10.15.0

ARG AWS_CLI_VERSION=1.16.94

USER root

RUN apt-get update && \
    apt-get install -y python-pip python-dev && \
    pip install --no-cache-dir awscli==${AWS_CLI_VERSION} python-magic

USER circleci
